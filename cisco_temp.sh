#!/bin/sh

IP=$1;

/usr/bin/snmpwalk -c public -v 1 ${IP} .1.3.6.1.4.1.9.9.13.1.3.1.3.1 | awk {print'$4'} |sed 's/"//g'