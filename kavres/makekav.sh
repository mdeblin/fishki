#!/bin/sh

# Переход в каталог, из которого запущен этот скрипт, актуально для запуска из crontab.
cd $(cd $(dirname $0) && pwd)

# Объявляем переменные.
krd10_iso="http://rescuedisk.kaspersky-labs.com/rescuedisk/updatable/kav_rescue_10.iso"

# Корневая папка tftp (в ней должен также быть загрузчик pxelinux.0 и т.п.),
# измените путь на свой.
tftproot_dir="/var"

# Папка назначения относительно tftproot_dir, измените путь на свой.
dest_dir="kav"

# Установите download_iso="1" если хотите, чтобы каждый раз скачивался свежий образ KRD10.
# Менять не рекомендуется, оставлено для отладки.
download_iso="0"

# Установите update_liveos="0" если не хотите менять настройки KRD10 или добавлять свои файлы.
update_liveos="1"

# Установите compress_lzma="1" если хотите сжать конечный образ.
# Эффективность сжатия не велика, по-умолчанию отключено.
compress_lzma="1"

# Скачиваем образ KRD10 если соответствующий флаг установлен.
echo && echo "STEP 1 : Downloading iso image..."
if [ "${download_iso}" = "1" ]; then
  if [ -e "kav_rescue_10.iso" ]; then
    mv -f kav_rescue_10.iso kav_rescue_10.iso.bak
  fi
  wget -nv ${krd10_iso}
  else
  echo "This step was skipped."
fi 

# Обновляем антивирусные базы
echo && echo "STEP 2 : Downloading anti-virus bases..."
# Запоминаем версию старых баз (sed нужен для избавления от символа "возврата каретки" в конце строки).
if [ -e "retranslator/bases/bases/av/kdb/i386/old/kdb.stt" ]; then
  old_base=`cat retranslator/bases/bases/av/kdb/i386/old/kdb.stt | sed 's/\x0D$//'`
else
  old_base="did not update yet"
fi

./retranslator/retranslator

# Запоминаем версию новых баз.
new_base=`cat retranslator/bases/bases/av/kdb/i386/old/kdb.stt | sed 's/\x0D$//'`
echo "Old base version : ${old_base}"
echo "New base version : ${new_base}"
# Короткая версия баз (только дата выпуска, без кол-ва известных вирусов) пригодится позднее.
base_ver=`echo ${new_base} | sed 's/.*;//'`

# Монтируем iso образ.
echo && echo "STEP 3 : Mounting iso image..."
mkdir iso && mount -o loop kav_rescue_10.iso iso/
# Запоминаем версию образа
disk_ver=`cat iso/rescue/KRD.VERSION | sed 's/; $//'`
echo "Disk version : ${disk_ver}"

# Распаковка rescue.igz.
echo && echo "STEP 4 : Decompressing rescue.igz..."
mkdir tmp
cp iso/boot/rescue.igz ./tmp/rescue.lzma
mkdir initrd
cd initrd && lzcat -S lz ../tmp/rescue.lzma | cpio -i --no-absolute-filenames && cd ../

# Самые главные патчи!
echo && echo "STEP 5 : Patching some files..."
# Блокируем поиск загрузочного диска, указываем скрипту монтировать содержимое LiveCD
# из готового образа, который соберем на шаге 7.

patch  -p0 -u -i init.patch
patch  -p0 -u -i dmsquash-live-root.patch

# Изменение настроек, добавление пользовательских файлов можно делать на этом этапе.
echo && echo "STEP 6 : Patching LiveOS..."
if [ "${update_liveos}" = "1" ]; then
  # Образ упакован с xz-компрессией! Распаковываем по-умолчанию в squashfs-root.
  ./squashfs4.2/squashfs-tools/unsquashfs iso/rescue/LiveOS/squashfs.img

  # Внутри еще один образ, монтируем его.
  mkdir mnt && mount -o loop -t auto squashfs-root/LiveOS/ext3fs.img mnt

  # Патч меняет кое-какие настройки (см. содержимое патча config.xml.patch ниже).
#patch -u -p0 -i config.xml.patch

  # Свои файлы можно кидать в ./mnt прямо СЕЙЧАС

  # Упаковываем измененный образ и заметаем следы.
  umount mnt && rm -rf mnt
  ./squashfs4.2/squashfs-tools/mksquashfs squashfs-root squashfs.img -comp xz
  rm -rf squashfs-root
else
  echo "This step was skipped."
fi

# Собираем образ загрузочного диска "с нуля".
echo && echo "STEP 7 : Building livecd.squash..."

mkdir -p livecd/rescue/help
cp iso/rescue/help/*.txt livecd/rescue/help/
cp -r iso/rescue/help/English livecd/rescue/help
cp -r iso/rescue/help/Russian livecd/rescue/help

cp iso/rescue/KRD.VERSION livecd/rescue/
cp iso/livecd livecd/

# Если было выбрано изменение настроек, пользуемся образом, собранным на предыдущем шаге...
if [ "${update_liveos}" = "1" ]; then
  mkdir livecd/rescue/LiveOS && mv squashfs.img livecd/rescue/LiveOS/
# ...В противном случае используем "родной".
else
  cp -r iso/rescue/LiveOS livecd/rescue
fi

# Собираем папку с обновленными базами!
mkdir -p livecd/rescue/bases/Stat
cp retranslator/bases/index/u0607g.xml livecd/rescue/bases/Stat/

cp retranslator/bases/bases/av/kdb/i386/*.ini livecd/rescue/bases/
cp retranslator/bases/bases/av/kdb/i386/*.kdc livecd/rescue/bases/
cp retranslator/bases/bases/av/kdb/i386/*.kdl livecd/rescue/bases/
cp retranslator/bases/bases/av/kdb/i386/*.xml livecd/rescue/bases/

mv livecd/rescue/bases/kdb-i386-0607g.xml livecd/rescue/bases/kdb-0607g.xml
cp retranslator/bases/bases/av/kdb/i386/old/kdb.stt livecd/rescue/bases/Stat/
cp retranslator/bases/bases/av/kdb/i386/old/kavbase.mft livecd/rescue/bases/

cp retranslator/bases/bases/av/emu/i386/* livecd/rescue/bases/
cp retranslator/bases/bases/av/qscan/i386/u/* livecd/rescue/bases/

# WindowsUnlocker тоже обновляем из зеркала.
mkdir livecd/rescue/bases/data
cp retranslator/bases/AutoPatches/rd/rd-0607g.xml livecd/rescue/bases/
cp retranslator/bases/AutoPatches/rd/windowsunlocker livecd/rescue/bases/data/

# Упаковываем.
./squashfs4.2/squashfs-tools/mksquashfs livecd initrd/livecd.squash

# Собираем обновленный rescue.igz.
echo && echo "STG3EP 8 : Building rescue.igz..."
if [ "${compress_lzma}" = "1" ]; then
  # с lzma сжатием
  echo "Lzma compress enabled."
  cd ./initrd && find . | cpio -o -Hnewc | lzma -c > ../rescue.igz && cd ../
  else
  # без lzma сжатия
  echo "Lzma compress disabled."
  cd ./initrd && find . | cpio -o -Hnewc > ../rescue.igz && cd ../
fi

# Перемещаем полученный образ, чистим временные папки.

smbmount //192.168.8.78/kav /var/kav  -o user=ldap,pass=ldapldap,dom=tts
echo && echo "STEP 9 : Moving image and cleaning temp dirs..."
if [ ! -d ${tftproot_dir}/${dest_dir} ]; then
  mkdir -p ${tftproot_dir}/${dest_dir}
fi

cp -f iso/boot/rescue ${tftproot_dir}/${dest_dir}/
mv -f rescue.igz ${tftproot_dir}/${dest_dir}/

rm -rf tmp
rm -rf livecd
rm -rf initrd
umount iso && rm -rf iso

# Создаем конфиг для pxelinux. Конфиг будет не простой, а красивый...
echo && echo "STEP 10 : Pxelinux config..."

echo "LABEL kaspersky_rescue_disk_10" > ${tftproot_dir}/${dest_dir}/menu.cfg
echo "MENU LABEL Kaspersky Rescue Disk ${disk_ver}-${base_ver}" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "KERNEL ${dest_dir}/rescue" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "APPEND initrd=${dest_dir}/rescue.igz root=live rootfstype=auto vga=791 init=/init kav_lang=ru udev liveimg doscsi nomodeset splash" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "TEXT HELP" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "Disk version : ${disk_ver}" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "New base version : ${new_base}" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "Old base version : ${old_base}" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "Rescue.igz build date : `date +%H:%M/%d.%m.%y`" >> ${tftproot_dir}/${dest_dir}/menu.cfg
echo "ENDTEXT" >> ${tftproot_dir}/${dest_dir}/menu.cfg

echo && cat ${tftproot_dir}/${dest_dir}/menu.cfg

umount /var/kav
