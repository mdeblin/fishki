[path]
#
# Location of retranslated files.
#
RetranslationPath=./bases/

#
# Directory where to create and use temporary files. 
#
TempPath=/tmp

[locale]
#
# Date formatting string. See date(1) man page for available formatting options.
#
DateFormat=%d-%m-%Y

#
# Time formatting string. See date(1) man page for available formatting options.
#
TimeFormat=%H:%M:%S

[updater.path]
#
# Path for backup of previous version of retranslated files.
#
BackUpPath=./backup/

#
# Path for store runing PID info. This file is used for prevent creation of another program's instances.
#
PidFile=/var/run/kav-retranslator.pid

[updater.options]
#
# Optional specification of components to be retranslated.
#
RetranslateComponentsList=EMU,KDB,QSCAN,RD

#
# Optional specification of OS filter.
#
os2=Linux

#
# Optional specification of instruction set filter.
#
instrset=kernel:i386;user:i386;


#
# Optional specification of application filter.
#
application=RD 10.*.*.*;

#
# Optional specification of build filter.
#
#build=1424

#
# Specifies the main update index.
# Note: You should not modify this option unless so advised
# by your customer support or expert user.
#
Index=u0607g.xml

#
# Specifies the root update path.
# Note: You should not modify this option unless so advised
# by your customer support or expert user.
#
IndexRelativeServerPath=index

#
# When set to "yes", keepup2date tries to perform an update from the specified
# UpdateServerUrl. If that fails, then it uses the default list of servers.
#
UseUpdateServerUrl=no

#
# If UpdateServerUrl is used, but the update fails, the updater continues by
# trying the servers in its default list. If this setting is set to "yes" (the
# (value assumed if not otherwise specified), keepup2date will not fall-back if
# UpdateServerUrl is not usable, and stop trying other servers.
#
UseUpdateServerUrlOnly=no

#
# Specifies a custom URL from where to download the updates.
# URL ca be:
#   http://location/ - to use HTTP protocol;
#   ftp://location/  - to use FTP protocol;
#   /local/path      - to use a local path.
#
UpdateServerUrl=

#
# When the updater uses its default list of server, this setting can select
# a preferred region, and the servers from that region will be tried first.
# The region is entered as two letter country code, e.g.  am, ar, at, az, be,
# bg, br, by, ca, cl, cn, cs, cz, de, ee, es, fr, gb, ge, gr, hk, hu, it, jp,
# kg, kr, kz, lt, lv, md, mx, nl, pl, ro, ru, th, tj, tm, tr, tw, ua, uk, us, uz.
#
# To obtain a list of the latest valid servers and region codes run 'keepup2date -s'.
#
RegionSettings=ru

#
# Network timeout setting, in seconds.
#
ConnectTimeout=20

#
# When set to "yes", updater does not display any information on operation
# progress.
#
KeepSilent=no

#
# When set to "yes" the updater will work only through the proxy, configured
# by ProxyAddress option, or from http_proxy or HTTP_PROXY environment vars.
# If this option is missing, the updater will try to find a proxy and use it
# or otherwise try to download directly.
#
UseProxy=no

#
# Specifies a proxy to be used for downloads, using the syntax:
#   http://[username:password@]url:port/
#
ProxyAddress=

#
# Whether or not to use passive ftp (you may need to turn it on
# if you are behind a firewall).
#
PassiveFtp=no

#
#
# Command to be executed when an retranslate completes successfully.
# The commands must return 0 (SUCCESS) or 1 (FAIL). If command is not successful, then
# it is logged as error.
#
PostRetranslateCmd=

[updater.report]
#
# When logging to file, defines whether to append new records
# to the report or overwrite the report at each session.
#
Append=no

#
# Report file name.
#
ReportFileName=./retranslator.log

#
# Defines how detailed the report entries should be.
# Detail levels are:
#  0 - Report only fatal errors. Only the errors, which result in program
#    termination, or failure to start, are reported; for example, if a
#    component failed to load a license file. Fatal errors have the 'F'
#    mark in the log file.
#  1 - Report all errors. Reports not only fatal errors, but also any error,
#    which affects program behavior, but not causing the component to
#    terminate. Non-fatal errors have the 'E' mark in the log file.
#  2 - Report all errors and warnings. Also reports important information,
#    which affects the component productivity, or may affect component ability
#    to work in future; for example, if the license is about to be expired.
#    Warnings have the 'W' mark in the log file.
#  3 - Report everything above, including the important information about the
#    component life cycle. This includes information about the component startup
#    and shutdown, the expiration date of the license, the last update date and so.
#    Informational messages have the 'I' mark in the log file.
#  4 - Report everything above, including the information about the current
#    component activity.  Activity messages have the 'A' mark in the log file.
#    Note that the total amount of activity information may be quite large.
#  9 - Report everything above, including the debugging information. Please use
#    this level if you are reporting bugs in the product. Do not forget to turn
#    it off afterwords. Debug messages have 'D' mark in the log file.
#
ReportLevel=3

