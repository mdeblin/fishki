#!/bin/bash

HOST=$1
TEMP=`snmpwalk -v 1 -c public ${HOST} .1.3.6.1.4.1.25728.8800.1.1.2.1|awk -F ':' 'END {printf("%d",$2)}'`
if [ $TEMP -gt 40 ]; then 
echo `date -R` "Температура в помещении ${TEMP}. Превышена масимальная температура! Запускаю аварийное отключение!" >> /var/log/syslog 2>&1
/opt/hosts.sh stop
fi
