#!/bin/bash
# $Id: mixedform,v 1.6 2010/01/13 10:47:35 tom Exp $


backtitle="IPMI"
returncode=0
ret=""
returncode=$?
exec 3>&1
value=`dialog  --title "BMC Configuration utility" --ok-label "Submit" \
           --backtitle "$backtitle" \
	  --insecure "$@" \
	  --mixedform "Here is a possible piece of a configuration program." \
20 50 0 \
	"NIC1 IP addr       :" 1 1	"0.0.0.0" 1 20 15 0 0 \
	"NIC2 IP addr       :" 2 1	"0.0.0.0" 2 20 15 0 0 \
	"Netmask            :" 3 1	"255.255.254.0"  3 20  15 0 0 \
	"Default gateway    :" 4 1	"192.168.0.250" 4 20 15 0 0 \
	"SNMP comunity      :" 5 1	"public" 5 20 15 0 0 \
	"Login              :" 6 1	"ipmiadm" 6 20 15 0 0 \
	"Password           :" 7 1	"Password~2003" 7 20 15 0 1 \
2>&1 1>&3`

set -- $value

ret=`ipmitool user set name 2 $6`
ret=`ipmitool user set password 2 $7`
ret=`ipmitool user priv 2 4 1`
ret=`ipmitool user priv 2 4 2`
ret=`ipmitool user test 2 16 $7`
echo "Test user name & password: "$ret
ret=`ipmitool user enable 2`
echo "Enable user: "$ret

ret=`ipmitool lan set 1 ipsrc static`
ret=`ipmitool lan set 1 arp respond on`
ret=`ipmitool lan set 1 auth ADMIN NONE,MD5,PASSWORD`
ret=`ipmitool lan set 1 access on`

ret=`ipmitool lan set 1 ipaddr $1`
echo $ret
ret=`ipmitool lan set 1 netmask $3`
echo $ret
ret=`ipmitool lan set 1 defgw ipaddr $4`
echo $ret
ret=`ipmitool lan set 1 snmp $5`
echo $ret


ret=`ipmitool lan set 2 ipsrc static`
ret=`ipmitool lan set 2 arp respond on`
ret=`ipmitool lan set 2 auth ADMIN NONE,MD5,PASSWORD`
ret=`ipmitool lan set 2 access on`

ret=`ipmitool lan set 2 ipaddr $2`
echo $ret
ret=`ipmitool lan set 2 netmask $3`
echo $ret
ret=`ipmitool lan set 2 defgw ipaddr $4`
echo $ret
ret=`ipmitool lan set 2 snmp $5`
echo $ret









