#!/usr/bin/perl

use LWP::UserAgent;
use Digest::MD5 qw(md5_hex);
use XML::LibXML;

if (@ARGV == 0 )
{
print "msa IP ";
exit;
}


$IP = $ARGV[0];
$counter=$ARGV[1];


my $md5_data = "monitor_!monitor";
my $md5_hash = md5_hex( $md5_data );
$ua = LWP::UserAgent->new;
$url = 'http://'.$IP.'/api/login/' . $md5_hash;
$req = HTTP::Request->new(GET => $url);
$res = $ua->request($req);
my $parser = XML::LibXML->new();
my $doc = $parser->parse_string( $res->content );
my $root = $doc->getDocumentElement;
my @objects = $root->getElementsByTagName('OBJECT');
my @props = $objects[0]->getElementsByTagName('PROPERTY');
my $sessionKey;
foreach my $prop ( @props ) {
my $name = $prop->getAttribute('name');
if( $name eq 'response' ) {
$sessionKey = $prop->textContent;
}
}


if ($ARGV[2] eq 'd'){
#Сбор статистики с vdisk
$url = 'http://'.$IP.'/api/show/vdisk-statistic';
$req = HTTP::Request->new(GET => $url);
$req->header('sessionKey' => $sessionKey );
$req->header('dataType' => 'ipa' );
$res = $ua->request($req);

my $parser1 = XML::LibXML->new();
my $doc1 = $parser1->parse_string( $res->content );
my $root1 = $doc1->getDocumentElement;
my @objects1 = $root1->getElementsByTagName('OBJECT');


    my @props1 = $objects1[$counter]->getElementsByTagName('PROPERTY');
    my $sessionKey1;
    foreach my $prop1 ( @props1 ) {
	my $Pname = $prop1->getAttribute("name");

        if ($Pname eq 'iops'){
		my $value = $prop1->getFirstChild()->getData();
	print  $value."\n";
        }
    }
}


if ($ARGV[2] eq 'c'){
#Сбор статистики с контроллеров
$url = 'http://'.$IP.'/api/show/controller-statistics';
$req = HTTP::Request->new(GET => $url);
$req->header('sessionKey' => $sessionKey );
$req->header('dataType' => 'ipa' );
$res = $ua->request($req);

    my $parser = XML::LibXML->new();
    my $doc = $parser->parse_string( $res->content );
    my $root = $doc->getDocumentElement;
    my @objects = $root->getElementsByTagName('OBJECT');
    my @props = $objects[$counter]->getElementsByTagName('PROPERTY');
    my $sessionKey;
    foreach my $prop ( @props ) {
    my $Pname = $prop->getAttribute("name");

    if ($ARGV[3] eq 'cpu'){
        if ($Pname eq 'cpu-load'){
	    my $Vname = $prop->getFirstChild()->getData();
	    print $Vname."\n";
        }
    }

    if ($ARGV[3] eq 'iops'){
        if ($Pname eq 'iops'){
		my $value = $prop->getFirstChild()->getData();
		print  $value."\n";
        }
	}
    }
}



$url2 = 'http://'.$IP.'/api/exit';
$req2 = HTTP::Request->new(GET => $url3);
$req2->header('sessionKey' => $sessionKey );
$req2->header('dataType' => 'api' );
$res2 = $ua->request($req2);
#print $res->content;




